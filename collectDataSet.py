import numpy as np
import cv2
import os
import math


def MakeDataSet(inputPath, splitingPercentage , dataShape):
    org_path = inputPath
    lablesNames = []
    train_data_input = []
    train_data_label = []
    test_data_input = []
    test_data_label = []
    if not os.path.isdir(org_path):
        raise ValueError("path {} doesn't exist make\
                          sure you select a valid path".format(org_path))
    if splitingPercentage<0 or splitingPercentage>100:
        raise ValueError("the percentage must be in range 0-100 ")

    try:
        if len(dataShape)!=2 :
            raise ValueError("select image shape(w,h)")
    except:
        raise ValueError("select image shape(w,h)")

    for folder in os.listdir(org_path):
        branch_dir = os.path.join(org_path, folder)
        totalSize = len(os.listdir(branch_dir))
        if totalSize == 0 :
            print "folder {} is empty".format(branch_dir)
            continue
        lablesNames.append(folder)
        count = 0
        trainingNums = int(math.ceil((splitingPercentage/100.0)*totalSize))
        for imgFile in os.listdir(branch_dir):
            imname = os.path.join(branch_dir, imgFile)
            image = cv2.imread(imname, 0)
            image = cv2.resize(image, (dataShape[0], dataShape[1]))
            image = np.reshape(image, dataShape[0]*dataShape[1])
            if count < trainingNums :
               train_data_input.append(np.float32(image) / 255.0)
               train_data_label.append(len(lablesNames) - 1)
            else:
                test_data_input.append(np.float32(image) / 255.0)
                test_data_label.append(len(lablesNames) - 1)
            count += 1
        print ("{} images added for {}....{} image for training and {} fortesting".format(count, lablesNames[-1] ,
                                                                                          trainingNums,count-trainingNums))

    print ("the registered datasets are ", lablesNames)

    X_Train = np.asarray(train_data_input , dtype=np.float32)
    X_Test  = np.asarray(test_data_input , dtype=np.float32)
    Y_Train = np.asarray(train_data_label, dtype=np.uint8)
    Y_Test  = np.asarray(test_data_label, dtype=np.uint8)
    return X_Train , X_Test , Y_Train , Y_Test , lablesNames

def Main():
    #MakeDataSet(path to dataset , percentage 0-100)
    X_Train, X_Test, Y_Train, Y_Test, lablesNames = \
        MakeDataSet("/home/mohamed/Desktop/tempDesktop/test (another copy)/dataSet", 50 , [100,100])
    for image in X_Test:
        img = np.asarray(image.reshape((100, 100))*255, dtype=np.uint8)
        cv2.imshow("test" , img)
        key = 0
        print("press m for display the next image and q to quit")
        while key&255 !=ord('m') and key&255 !=ord('q'):
            key = cv2.waitKey(100)
        if key&255 ==ord('q'):
            break

    cv2.destroyAllWindows()
    

if __name__ == '__main__':
    Main()




